//
// Created by brian on 12/29/15.
//

#ifndef AX_ARM_CONTROLLER_PHYARMCONTROLLER_H
#define AX_ARM_CONTROLLER_PHYARMCONTROLLER_H


#include <ros/node_handle.h>
#include <dynamixel_msgs/JointState.h>
#include <modules/VrepArmController.h>
#include <modules/JointData.h>
#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

class PhyArmController {


public:

    /*init*/
    void init();

    /*action function*/
    void setJointPosition(int jointNum, double position);
    void setJointPosition(string jointName);
    void setJointsPosition(vector<PhyJoint> joints);
    void setToCalibrationPosition();

        /*WARNING: joint name in vrep and .yaml should be the same*/
        /*also check the joint name defined in source code*/
    void mirrorJointsPositionFromVrep(VrepArmController &vrepArmController, PointOffset pointOffset);

    /*help function*/
    void printJointsInfo();
    void printJointsIndex();

    /*grabber function*/
    void openGrabberForBall();
    void closeGrabberForBall();

    //    Contructor
    PhyArmController(ros::NodeHandle nodeHandle);

    void jointStateCallback(const dynamixel_msgs::JointStateConstPtr& jointState);


private:
    ros::NodeHandle nodeHandle;

    std::vector<std::string> jointsControllerNameVector;
    std::vector<std::string> jointsNameVector;

    std::vector<ros::Subscriber> subscriberList;
    std::vector<ros::Publisher> publisherList;
    std::map<string, ros::Publisher> publisherMap;

    std::map<string,dynamixel_msgs::JointState> jointsStateMap;



};


#endif //AX_ARM_CONTROLLER_PHYARMCONTROLLER_H
