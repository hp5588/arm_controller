//
// Created by brian on 12/29/15.
//

#ifndef AX_ARM_CONTROLLER_CENTRALCONTROLLER_H
#define AX_ARM_CONTROLLER_CENTRALCONTROLLER_H

#include <ros/node_handle.h>

#include <modules/VrepArmController.h>
#include <modules/PhyArmController.h>
#include <modules/BallTracker.h>
#include <modules/UbotController.h>


/*z:camera depth*/
#define GRAB_REGION_Z_MAX 0.50
#define GRAB_REGION_Z_MIN 0.47
#define GRAB_REGION_X_SIDERANGE 0.1
#define GRAB_REGION_X_MID 0.15

#define CHEAT_Z 0.05

#define KINECT_X -0.08

#define HALF_GRAB_REGION_X_WIDTH GRAB_REGION_X_WIDTH/2.0
#define PREFER_GRAB_DISTANCE (GRAB_REGION_Z_MIN+GRAB_REGION_Z_MAX)/2 /*m*/
#define PREFER_GRAB_X GRAB_REGION_X_MID
#define PREFER_GRAB_RADIAN atan(PREFER_GRAB_X/PREFER_GRAB_DISTANCE)+0.09





class CentralController {
public:
    CentralController(ros::NodeHandle nodeHandle, PhyArmController &phyArmController,
                      VrepArmController &vrepArmController, BallTracker &ballTracker);
    void run();
    void init();
    void calibrate();


private:
    /*ros node handle*/
    ros::NodeHandle nodeHandle;

    /*controller obj*/
    PhyArmController *phyArmController;
    VrepArmController *vrepArmController;
    UbotController *ubotController ;

    BallTracker *ballTracker;

    /*variables*/
    geometry_msgs::Vector3 calibrateDifferentce;

    /*action algorithm*/
    bool move (geometry_msgs::Point ballPoint);
    bool grabBall (geometry_msgs::Point ballPoint);

    /*help function*/
    double directionDifferenceFromPoint(geometry_msgs::Point ballPoint);  /*radian*/
    double distanceFromPoint(geometry_msgs::Point ballPoint);  /*m*/

    void printCentralControllerState(int state);

    /*check function*/
    bool ballPointInRegion(geometry_msgs::Point ballPoint);



};




#endif //AX_ARM_CONTROLLER_CENTRALCONTROLLER_H
