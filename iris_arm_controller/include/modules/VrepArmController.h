//
// Created by brian on 12/29/15.
//

/*
 * Start Vrep before calling Arm Controller*/

#ifndef AX_ARM_CONTROLLER_ARMCONTROLLER_H
#define AX_ARM_CONTROLLER_ARMCONTROLLER_H
#define ARM_ERROR -99

#define TARGET_INIT_X 0
#define TARGET_INIT_Y 0.631
#define TARGET_INIT_Z 0.36

#include <ros/node_handle.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PoseStamped.h>
#include "JointData.h"


using  namespace std;

class VrepArmController {


public:
    VrepArmController(ros::NodeHandle nodeHandle);
    bool init();
    bool setTargetPointRelativeToKinectReference(geometry_msgs::Point point);
    bool setTargetPointRelativeToWorldCoord(geometry_msgs::Point point);
    bool setTargetInit();
    geometry_msgs::PoseStamped getTargetPoseRelativeToKinectReference();

    bool getJointsPosition(map<string, VrepJoint> &jointsInfo);
    double getJointsPosition(int jointNum);

    /*getter*/
    map<string, VrepJoint> getJointInfoMap();

    void printJointsInfo();


private:
    ros::NodeHandle nodeHandle;

    int32_t ballHandle;
    int32_t kinectReferenceHandle;
    map<string, VrepJoint> jointInfoMap;
    map<string, VrepJoint> targetInfoMap;


    ros::ServiceClient serviceClient_simRosGetObjectHandle ;
    ros::ServiceClient serviceClient_simRosGetObjectPose ;
    ros::ServiceClient serviceClient_simRosSetObjectPosition ;
    ros::ServiceClient serviceClient_simRosGetJointMatrix ;
    ros::ServiceClient serviceClient_simRosGetJointState ;
    ros::ServiceClient serviceClient_simRosStartSimulation ;

    void printMap(std::map<string, VrepJoint> &theMap);

};







#endif //AX_ARM_CONTROLLER_ARMCONTROLLER_H
