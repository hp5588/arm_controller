//
// Created by brian on 12/29/15.
//

#ifndef AX_ARM_CONTROLLER_BALLTRACKER_H
#define AX_ARM_CONTROLLER_BALLTRACKER_H



#include <ros/node_handle.h>
#include <opencv2/opencv.hpp>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/point_cloud_conversion.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/conversions.h>

#include <cv_bridge/cv_bridge.h>

//Mark as red point if higher than this value
#define R_LOW_THRESHOlD_VALUE 100

//Remove the point from red Mat if higher than this value
#define G_LOW_THRESHOlD_VALUE 100
#define B_LOW_THRESHOlD_VALUE 200

#define G_HIGH_THRESHOlD_VALUE 200
#define B_HIGH_THRESHOlD_VALUE 200




using namespace cv;
using namespace std;

class BallTracker {

public:
    BallTracker(ros::NodeHandle nodeHandle);

    void init();

    /*return 1 if obj is found*/
    void trackObj(geometry_msgs::Point &objPoint, bool &tracked);



private:
    /*ros handle*/
    ros::NodeHandle nodeHandle;

    /*variables*/
    RNG rng;
    float upperHue, lowerHue;


    /*callback data*/
    cv_bridge::CvImagePtr cvBridgeRgbImagePtr;
    cv_bridge::CvImagePtr cvBridgeDepthImage;
    sensor_msgs::PointCloud2 pointCloud2;

    /*subscriber*/
    ros::Subscriber rgbRawSubscriber;
    ros::Subscriber depthPointCloudSubscriber;
    ros::Subscriber depthImageSubscriber;

    /*callback function*/
    void rgbRawTopicCallback(const sensor_msgs::ImageConstPtr& rgbRawImage);
    void depthPointCloudCallback(const sensor_msgs::PointCloud2ConstPtr &depthPointCloud2Image);
    void depthCallback(const sensor_msgs::ImageConstPtr& depthRawImage);

    /*test function*/
    void trackbarCallback();



    /*image processing method*/
    Mat filterRedPass(Mat inFrame);

    double pointDistance(Point2d pointA, Point2d pointB);






};



#endif //AX_ARM_CONTROLLER_BALLTRACKER_H
