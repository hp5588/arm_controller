//
// Created by brian on 2/3/16.
//

#ifndef IRIS_ARM_CONTROLLER_UBOTCONTROLLER_H
#define IRIS_ARM_CONTROLLER_UBOTCONTROLLER_H

using namespace std;
#include <ros/node_handle.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Byte.h>
#include <math.h>

#define UBOT_CMD_TOPIC_NAME "cmd_vel"
#define UBOT_STATUS_TOPIC_NAME "ubot_status"

#define UBOT_STATUS_MOVING 0x33
#define UBOT_STATUS_STILL 0x32



class UbotController {
public:
    UbotController(ros::NodeHandle nodeHandle);
    bool init();

    /*ubot status*/
    int8_t status;

    void turn(double radian);
    void forward(float distance); //unit:(m)

    void move (double radian, float distance);
private:
    /*ros*/
    ros::NodeHandle nodeHandle;
    ros::Publisher ubotCmdPublisher;
    ros::Subscriber ubotStatusSubscriber;



    /*topic callback function*/
    void  ubotStatusCallback(const std_msgs::ByteConstPtr &ubotStatus);

};


#endif //IRIS_ARM_CONTROLLER_UBOTCONTROLLER_H
