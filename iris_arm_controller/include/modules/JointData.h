//
// Created by brian on 12/29/15.
//

#ifndef AX_ARM_CONTROLLER_JOINTDATA_H
#define AX_ARM_CONTROLLER_JOINTDATA_H

#include <tiff.h>

struct VrepJoint {
    int32 handle;
    float position;
};

struct PhyJoint {
    int jointNum;
    double position;
};

struct Joint{
    int handle;
    float position;
};

struct PointOffset{
    float x;
    float y;
    float z;
};
#endif //AX_ARM_CONTROLLER_JOINTDATA_H
