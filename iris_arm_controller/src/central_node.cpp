#include "ros/ros.h"


#include <cstdio>
#include <sstream>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include <modules/VrepArmController.h>
#include <modules/PhyArmController.h>
#include <modules/BallTracker.h>
#include <modules/CentralController.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/JointState.h>

#include <vrep_common/simRosStartSimulation.h>
#include <vrep_common/simRosGetObjectHandle.h>
#include <vrep_common/simRosSetObjectPosition.h>
#include <vrep_common/simRosGetJointMatrix.h>
#include <vrep_common/simRosGetJointState.h>

#include <boost/thread.hpp>

#include <math.h>


using namespace std;

void prompt(ros::NodeHandle &n, VrepArmController &vrepArmController, PhyArmController &phyArmController,
            BallTracker &ballTracker, CentralController &centralController);
void callbackSpin();

void printHelp();

void initilArmPosition(PhyArmController &phyArmController);
void trackingThread(BallTracker &ballTracker);





geometry_msgs::Point point;
bool tracked;



int main(int argc, char **argv)
{
  /*ROS initialization*/
  ros::init(argc, argv, "ArmController");
    ros::NodeHandle n;
    ros::Rate loop_rate(10);


    /*bash to ask for permission(note: modify password for different user)*/
    char packagePathChar[100];
    FILE *file = popen("rospack find iris_arm_controller","r");
    fgets(packagePathChar,100,file);
    string packagePath = string(packagePathChar).erase(string(packagePathChar).length()-1) + "/cmd";
    system(packagePath.c_str());
    pclose(file);

  /*Topic Publisher*/
//  ros::Publisher chatter_pub = n.advertise<std_msgs::Float64>("/dual_motor_controller1/command", 1000);

  /*Topic Listener*/

  /*Service Clients*/

  /*Initiate Arm Controller*/
    VrepArmController vrepArmController(n);
    vrepArmController.init();

    /*Initiate Ball Tracker*/
    BallTracker ballTracker(n);
    ballTracker.init();

    /*Initiate Physical Arm Controller*/
    PhyArmController phyArmController(n);
    phyArmController.init();

    /*Initiate Central Controller*/
    CentralController centralController(n ,phyArmController,vrepArmController,ballTracker);
    centralController.init();


    /*open threads*/
//    boost::thread promtThreadHandle(prompt,vrepArmController,phyArmController);
    boost::thread callbackSpinThreadHandle(callbackSpin);






    sleep(1);


while (ros::ok())
  {
//    TODO: why can't access jointsStateMap if prompt run new thread other than main()
    prompt(n, vrepArmController, phyArmController, ballTracker, centralController);
    sleep(1);
//      ros::spinOnce();
  }
   return 0;

}




void prompt(ros::NodeHandle &n, VrepArmController &vrepArmController, PhyArmController &phyArmController,
            BallTracker &ballTracker, CentralController &centralController) {
    std::string commands;


//    initilArmPosition(phyArmController);

//    cout << "commands entered: " << commands <<endl;
    while (ros::ok()) {
        std::cout << "Enter Commands: " ;
        cin >> commands ;
        if (commands.compare(0, 1, "-") == 0) {
            for (int position = 1; position < commands.length(); position++) {
                char cmd = commands.at(position);
                switch (cmd) {
#pragma  command- run central controller
                    case 'r':{
//                        /*Initiate Central Controller*/
//                        CentralController centralController(n ,phyArmController,vrepArmController,ballTracker);
//                        centralController.init();

                        /*run (while loop)*/
                        centralController.run();

                        break;
                    }
                    /*calibration*/
                    case 'c':{
//                        /*Initiate Central Controller*/
//                        CentralController centralController(n ,phyArmController,vrepArmController,ballTracker);
//                        centralController.init();

                        centralController.calibrate();
                        break;
                    }
                    case 'z':{
//

                        vrepArmController.setTargetInit();
                        break;
                    }


#pragma  command- set vrep target position
                    case 's': {
                        //collect parameter
                        float x, y, z = 0;

                        cout << "x:";
                        cin >> x; //cout << endl;
                        cout << "y:";
                        cin >> y; //cout << endl;
                        cout << "z:";
                        cin >> z; //cout << endl;

                        geometry_msgs::Point point;
                        point.x = x;
                        point.y = y;
                        point.z = z;


                        vrepArmController.setTargetPointRelativeToKinectReference(point);
//                        vrepArmController.setTargetPointRelativeToWorldCoord(point);


                        break;
                    }

#pragma  command- get vrep joint state
                    case 'j': {
                        //read the parameter of each joint from vrep
                        map<string, VrepJoint> jointsInfo;
                        vrepArmController.getJointsPosition(jointsInfo);
                        vrepArmController.printJointsInfo();


                        break;
                    }
#pragma command- print all phy joints info
                    case 'a': {
                        phyArmController.printJointsInfo();
                        break;
                    }

                    case 'n': {
                        phyArmController.printJointsIndex();
                        break;
                    }

                    case 'i': {
                        initilArmPosition(phyArmController);
                        break;
                    }

                    case 'd': {
                        phyArmController.printJointsInfo();
                        phyArmController.printJointsIndex();
                        int index;
                        double position;
                        cout<< "controller index:";
                        cin >> index;
                        cout<< "set position to:";
                        cin >> position;

                        phyArmController.setJointPosition(index,position);
                        break;
                    }
#pragma command- video processing

                    case 'v': {
//                        TODO::point is empty now
                        geometry_msgs::Point point;
                        bool tracked = false;
                        namedWindow( "src", CV_WINDOW_AUTOSIZE );

//                        boost::thread *thread = new boost::thread(&trackingThread,ballTracker);
//                        thread->join();
                        while(1){
                            ballTracker.trackObj(point, tracked);
                            if (waitKey(1) > -1) {
                                destroyAllWindows();
                                break;
                            }
                        }

                        break;
                    }

                    case 'o':{
                        /*follow object*/
                        geometry_msgs::Point initPoint ;
                        initPoint.x = -0.2;
                        initPoint.z = 0.5;
                        while (1){
                            geometry_msgs::Point point;
                            bool tracked = false;
                            ballTracker.trackObj(point, tracked);
                            if (tracked) {
                                vrepArmController.setTargetPointRelativeToKinectReference(point);
                                phyArmController.mirrorJointsPositionFromVrep(vrepArmController, PointOffset{0, 0, 0});
                            }
                            if (waitKey(100) > -1) {
                                destroyAllWindows();
                                break;
                            }
                        }
                    }



#pragma command- exit the node

                    case 'e': {
                        ros::shutdown();
                        break;
                    }

                    case 'h': {
                        printHelp();
                        break;
                    }


                    default: {
                        cout << "commad not found" << endl;
                        printHelp();
                    }
                }
            }
        }
    }

//    give time to calllback funtions
//    ros::spin();

}

void printHelp(){
    cout << "---------------help---------------"<<endl;
    cout << " | CentralController |" <<endl;
    cout << "  " << "-r " << "run central controller" <<endl;
    cout << "  " << "-c " << "calibrate physical arm" <<endl;
    cout <<endl;
    cout << " | VreArmController |" <<endl;
    cout << "  " << "-s " << "manually set target position in vrep" <<endl;
    cout << "  " << "-j " << "print arm's joint state in vrep" <<endl;
    cout <<endl;
    cout << " | PhyArmController |" <<endl;
    cout << "  " << "-a " << "print joint state in physical arm" <<endl;
    cout << "  " << "-n " << "print joint index in physical arm" <<endl;
    cout << "  " << "-i " << "set physical arm in initial position" <<endl;
    cout << "  " << "-d " << "set physical arm joint position with index " <<endl;
    cout <<endl;
    cout << " | BallTracker |" <<endl;
    cout << "  " << "-v " << "track object" <<endl;
    cout << "  " << "-o " << "phy arm to follow track object" <<endl;
    cout <<endl;
    cout << " | Other Function |" <<endl;
    cout << "  " << "-e " << "end current node" <<endl;
    cout << "  " << "-h " << "help" <<endl;







}


void initilArmPosition(PhyArmController &phyArmController) {

//   defined in PhyArmController init()
//   0  "tilt_joint"
//   1  "dual_motor_grabber_joint",
//   2  "rotate_joint",
//   3  "dual_motor2_joint",
//   4  "dual_motor1_joint"

    double position[] =  { 0 , 2.4 , 0 , 1.5 , 1.4 };

    for (int i = 0 ; i < 5 ; i++) {

        phyArmController.setJointPosition(i ,position[i]);
    }

}


void trackingThread(BallTracker &ballTracker) {
    int count = 0;
    while (1) {
        count++;
        cout<< "thread count "<<count <<endl;
        ballTracker.trackObj(point, tracked);
//        boost::posix_time::milliseconds sleeptime(20);
//        boost::this_thread::sleep(sleeptime);
    }

}

void callbackSpin()
{
    while(1)
    {

//        cout<< "spinOnce" <<endl;
        ros::spinOnce();
        boost::posix_time::milliseconds sleeptime(20);
        boost::this_thread::sleep(sleeptime);
    }
}