//
// Created by brian on 2/3/16.
//

#include <modules/UbotController.h>

UbotController::UbotController(ros::NodeHandle nodeHandle) {
    this->nodeHandle = nodeHandle;

}

bool UbotController::init() {
    ubotCmdPublisher = nodeHandle.advertise<geometry_msgs::Twist>(UBOT_CMD_TOPIC_NAME,10);
    ubotStatusSubscriber = nodeHandle.subscribe(UBOT_STATUS_TOPIC_NAME,10,&UbotController::ubotStatusCallback,this);

    return true;

}

void UbotController::turn(double radian) {

    /*TODO:debug*/
    cout << "turn: "<< radian << " rad"<<endl;
    geometry_msgs::Twist msg;
    msg.angular.z = radian;

    ubotCmdPublisher.publish(msg);
//    ros::spinOnce();
//    ros::Rate loop_rate(10);
//    loop_rate.sleep();

}

void UbotController::forward(float distance) {
    geometry_msgs::Twist msg;
    msg.linear.x = distance;

    ubotCmdPublisher.publish(msg);
//    ros::spinOnce();
//
//    ros::Rate loop_rate(10);
//    loop_rate.sleep();


}

void UbotController::ubotStatusCallback(const std_msgs::ByteConstPtr &ubotStatus) {
    status = ubotStatus->data;

}

void UbotController::move(double radian, float distance) {
    /*TODO:debug*/
    cout << "turn: "<< radian << " rad"<<endl;
    cout << "distance: "<< distance << " m"<<endl;

    geometry_msgs::Twist msg;
    msg.angular.z = radian;
    msg.linear.x = distance;
    ubotCmdPublisher.publish(msg);


}
