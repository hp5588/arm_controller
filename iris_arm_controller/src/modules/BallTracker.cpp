//
// Created by brian on 12/29/15.
//

#include <modules/BallTracker.h>


BallTracker::BallTracker(ros::NodeHandle nodeHandle) {
    this->nodeHandle = nodeHandle;


}


void BallTracker::init() {
    /*subscribe topic*/
    rgbRawSubscriber = nodeHandle.subscribe("/camera/rgb/image_color",30,&BallTracker::rgbRawTopicCallback,this);
    depthPointCloudSubscriber = nodeHandle.subscribe("/camera/depth/points",30,&BallTracker::depthPointCloudCallback,this);
    depthImageSubscriber = nodeHandle.subscribe("/camera/depth/image",30,&BallTracker::depthCallback,this);

    /*initialize variables*/
    rng = rng(12345);
}

void BallTracker::trackObj(geometry_msgs::Point &objPoint, bool &tracked) {


    int defaultLowerHue[] = {143};
    int defaultUpperHue[] = {192};

    /*create UI*/
    createTrackbar("lowerHue","src",defaultLowerHue,360);
    createTrackbar("upperHue","src",defaultUpperHue,360);

    /*variables*/
    const int contoursBufferSize = 15;  //0.5sec base on 30fps
    const int goodFrameSize = 5;
    const int trackThreshold = 5;
    const float allowedDistance = 20;//5
    const float extremeDistance = 50;
    const int nullFrameCountThreshold = 10;
    const float minContourArea = 100;

    static bool objTracked = false;

//    vector<Point2d> contoursAreaVector;

    /*setup parameter*/
//    lowerHue = 146;
//    upperHue = 192;


    /*read parameter*/
    lowerHue = getTrackbarPos("lowerHue", "src");
    upperHue = getTrackbarPos("upperHue", "src");

    /*parameter*/
    Scalar redLower = Scalar(lowerHue, 86, 50);
    Scalar redUpper = Scalar(upperHue, 255, 255);

    /*while(1) loop*/


        /*variables*/
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        Mat src, mask = Mat(cvBridgeRgbImagePtr->image.rows, cvBridgeRgbImagePtr->image.cols, CV_8U, Scalar(0, 0, 0));
        static Point trackedPoint;

        /*!!!important!!! copying */
        cvBridgeRgbImagePtr->image.copyTo(src);


        GaussianBlur(src, src, Size(11, 11), 0, 0);
        cvtColor(src, src, COLOR_BGR2HSV);
        inRange(src, redLower, redUpper, mask);
//        imshow( "mask_org", mask );

        /*eliminate small point*/
        erode(mask, mask, Mat(), Point(-1, -1), 4);
        dilate(mask, mask, Mat(), Point(-1, -1), 2);
        imshow("erDiMask", mask);

        /*find contours*/
        findContours(mask, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

        /*find contour with max area*/
        double maxContourArea = 0;
        vector<Point> maxContour;
        for (vector<Point> contour : contours) {
            double cArea = contourArea(contour, false);
            if (cArea > maxContourArea) {
                if (cArea > minContourArea) {
                    maxContourArea = cArea;
                    maxContour = contour;
                }
            }
        }


        Point2f center;
        float radius;
        static int nullFrameCount = 0;
//        exception handle
        if (maxContour.size()) {

            /*zero the count*/
            nullFrameCount = 0;

            /*find min circle*/
            minEnclosingCircle(maxContour, center, radius);


            /*find moment*/
            Moments m = moments(maxContour);
            Point2d mCenter = Point2d(m.m10 / m.m00, m.m01 / m.m00);




            /*average filter*/
            static vector<Point> bufferFrameVecter;
            static vector<Point> goodFrameVecter;

            if (bufferFrameVecter.size() < contoursBufferSize) {
                bufferFrameVecter.push_back(mCenter);
            }
            else {
                bufferFrameVecter.erase(bufferFrameVecter.begin());
                bufferFrameVecter.push_back(mCenter);
            }


//            cout << "distance: " <<this->pointDistance(mCenter,*bufferFrameVecter.end())<<endl;

            if (objTracked) {
                //Check a frame before if it's very different
                static int badPointCount = 0;
                if (this->pointDistance(mCenter, *(bufferFrameVecter.end() - 2)) > allowedDistance) {
                    //if so, then check with the good record
                    double averageDistance = 0;
//                    for (Point2d goodPoint : goodFrameVecter){
//                        averageDistance += this->pointDistance(goodPoint,mCenter);
//                    }
//                    averageDistance /= goodFrameVecter.size();
                    /*TODO:testing*/
                    averageDistance = this->pointDistance(*goodFrameVecter.end(), mCenter);
//                    cout << "distance: " << averageDistance << endl;

                    if (averageDistance < allowedDistance) {
                        //this is a good point -> keep it
                        goodFrameVecter.erase(goodFrameVecter.begin());
                        goodFrameVecter.push_back(mCenter);
                        trackedPoint = mCenter;

                    } else {
                        //bad point again
                        //obj out of track if 5 bad frame continuously appear
                        badPointCount++;
                        cout << "bad count" << badPointCount << " moment: " << mCenter << endl;

                        if (badPointCount > 5) {
                            objTracked = false;
                            badPointCount = 0;
                            trackedPoint = Point(0, 0);
                            cout << "obj untracked" << endl;
                        }

                        //give up the point if it's right away if it's too far
                        //but might be still in tracked condition
                        if (averageDistance > extremeDistance) {
                            //not to update tracked point
                        }
                    }
                } else {
                    //good point, so update good point vector
                    goodFrameVecter.erase(goodFrameVecter.begin());
                    goodFrameVecter.push_back(mCenter);
                    trackedPoint = mCenter;
                    badPointCount = 0;

                }
//                circle(src, *goodFrameVecter.end(), 5, Scalar(255, 0, 255), -1);
            } else {
                if (bufferFrameVecter.size() >= contoursBufferSize) {
                    static int goodCount = 0;
                    for (Point2d bufferPoint : bufferFrameVecter) {
                        //Check if distance is quite consistent
                        if (this->pointDistance(mCenter, bufferPoint) > allowedDistance) {
                            //bad point and zero the goodCount
                            goodCount = 0;
                        } else {
                            goodCount++;
                        }

                    }

                    if (goodCount > trackThreshold) {
                        objTracked = true;
                        goodCount = 0;

                        //add points to good vector
                        goodFrameVecter = vector<Point>();
                        goodFrameVecter.insert(goodFrameVecter.begin(), bufferFrameVecter.end() - goodFrameSize,
                                               bufferFrameVecter.end());
                        trackedPoint = mCenter;
                        cout << "obj tracked" << endl;

                    }

                } else {
                    //wait until have enough buffer
                }

            }
            if (objTracked) {
                circle(src, trackedPoint, 5, Scalar(0, 0, 255), -1);
                int x = (int) mCenter.x;
                int y = (int) mCenter.y;

                pcl::PCLPointCloud2 pclCloud2;
                pcl::PointCloud<pcl::PointXYZ> pclCloud;
                pcl::fromROSMsg(pointCloud2, pclCloud);
                vector<pcl::PointXYZ> areaPoints;
                if (pclCloud.isOrganized()) {
                    int halfAvgContourSideLenght = sqrt(maxContourArea)/2;
                    /*check point in avgContourSideLenght*avgContourSideLenght rectangular area to find closest point */
                    for (int j = -halfAvgContourSideLenght; j < halfAvgContourSideLenght; ++j) {
                        for (int i = -halfAvgContourSideLenght; i < halfAvgContourSideLenght; ++i) {
                            int xp = x + i;
                            int yp = y + j;
                            /*check if within range*/
                            if ((xp > 0) && (yp >0))
                                if ((xp < pclCloud.width) && (yp < pclCloud.height))
                                    areaPoints.push_back(pclCloud.at(xp, yp));
                        }
                    }


                    static pcl::PointXYZ closestPoint;
                    double minZ = 10;
                    for (pcl::PointXYZ point : areaPoints) {
                        if ((point.z < minZ) && (point.z!=0)) {
                            minZ = point.z;
                            closestPoint = point;
                        }
                    }

                    /*return tracked data*/
                    objPoint.x = closestPoint.x;
                    objPoint.y = closestPoint.y;
                    objPoint.z = closestPoint.z;
                    tracked = true;



                    stringstream buffer;
                    buffer << closestPoint;
                    putText(src, buffer.str(), Point(0, 100), 0, 1, Scalar(0.255, 255), 1, 8, false);
                }


            } else{
                tracked = false;
                objPoint.x = 0;
                objPoint.y = 0;
                objPoint.z = 0;
            }



            /*mark the ball on the frame*/
            circle(src, center, radius, Scalar(0, 255, 255), 2);
//            circle(src, mCenter, 5, Scalar(0, 0, 255), -1);

            /*put contour size info*/
            string text = "contour size:";
            text += to_string(maxContourArea);
            putText(src, text, Point(0, src.rows), 0, 1, Scalar(0.255, 255), 1, 8);

        } else {
            //set to untracked if too many frames are missed
            nullFrameCount++;
            if (nullFrameCount > nullFrameCountThreshold) {
                objTracked = false;
                nullFrameCount = 0;
                cout << "obj untracked" << endl;
            }

        }


    imshow("src", src);




}

/*unused function*/
Mat BallTracker::filterRedPass(Mat inFrame) {
    int fSize = inFrame.cols * inFrame.rows;

    Mat channelSplit[3];
    Mat filteredFrame = Mat(inFrame.rows, inFrame.cols, CV_8U, Scalar(0,0,0));
    Mat redMarked = Mat(inFrame.rows, inFrame.cols, CV_8U , Scalar(0,0,0));

    cv::split(inFrame, channelSplit);
    //cout<<"type:"<<inFra//me.type()<<endl;
    //uint8_t temp =  channelSplit[2].at<uint8_t>(2000));
    //std::cout<< "Value at :"<< (0xFF&channelSplit[2].at<uint8_t>(307000)) <<endl;


    for (int ps = 0; ps < fSize; ps++) {
        uint8_t bValue = channelSplit[0].at<uint8_t>(ps);
        uint8_t gValue = channelSplit[1].at<uint8_t>(ps);
        uint8_t rValue = channelSplit[2].at<uint8_t>(ps);


        //Default :the point value in filteredFrame is zero
        if (rValue > R_LOW_THRESHOlD_VALUE) {
            if ((gValue >G_HIGH_THRESHOlD_VALUE) && (bValue > B_HIGH_THRESHOlD_VALUE)) {
                /*keep reflection*/
                filteredFrame.at<uint8_t>(ps) = rValue;
            } else{
                redMarked.at<uint8_t>(ps) = 255;
                if ((gValue > G_LOW_THRESHOlD_VALUE) || (bValue > B_LOW_THRESHOlD_VALUE)) {
                    //Mark point as nothing
                    filteredFrame.at<uint8_t>(ps) = 0;
                } else {
                    //Mark point as white representing red point
                    filteredFrame.at<uint8_t>(ps) = rValue;
                    //filteredFrame.at<uint8_t>(ps) = 255;
                }
            }
        }
    }

//    imshow("redChannel", channelSplit[2]);
//    imshow("greenChannel", channelSplit[1]);
//    imshow("blueChannel", channelSplit[0]);
    return filteredFrame;

}

void BallTracker::rgbRawTopicCallback(const sensor_msgs::ImageConstPtr &rgbRawImage) {
    cvBridgeRgbImagePtr = cv_bridge::toCvCopy(rgbRawImage, sensor_msgs::image_encodings::BGR8);
    if ((rgbRawImage->width<1) ||(rgbRawImage->height<1 ))
        cout << "Null Image" <<endl;




//    this->rgbImagePtr = *rgbRawImage;
}

void BallTracker::depthPointCloudCallback(const sensor_msgs::PointCloud2ConstPtr &depthPointCloud2Image) {
    this->pointCloud2 = *depthPointCloud2Image;
}

void BallTracker::depthCallback(const sensor_msgs::ImageConstPtr &depthRawImage) {
    cvBridgeDepthImage = cv_bridge::toCvCopy(depthRawImage, sensor_msgs::image_encodings::TYPE_32FC1);
}

double BallTracker::pointDistance(Point2d pointA, Point2d pointB) {
    double xDff = pointA.x - pointB.x;
    double yDff = pointA.y - pointB.y;
    double diff = sqrt(pow(xDff,2) + pow(yDff,2));

//    cout << "pointA" << pointA << " pointB"<< pointB<< " diff:" <<diff<< endl;

    return diff;
}


