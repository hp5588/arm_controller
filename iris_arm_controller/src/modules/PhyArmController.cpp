//
// Created by brian on 12/29/15.
//

#include <modules/PhyArmController.h>
#include <dynamixel_msgs/JointState.h>
#include <std_msgs/Float64.h>

PhyArmController::PhyArmController(ros::NodeHandle nodeHandle) {
    this->nodeHandle = nodeHandle;

}

void PhyArmController::init() {


  /*Joint name list*/
    /*name refer to the /config/dynamixel_joint_controllers.yaml*/
    string jointControllerNameList[] = {
            "sway_controller",
            "dual_motor_grabber_controller",
            "rotate_controller",
            "stretch1_controller",
            "stretch0_controller"};

    string jointNameList[] = {
            "sway_joint",
            "dual_motor_grabber_joint",
            "rotate_joint",
            "stretch1_joint",
            "stretch0_joint"};


    string serviceAvaliable[] = {"set_speed","set_torque_limit","torque_enable"};


//    TODO: what the wrong with the index!???? doesn't work with  [4]
    jointsControllerNameVector = vector<string>(&jointControllerNameList[0], &jointControllerNameList[5]);
    jointsNameVector = vector<string>(&jointNameList[0], &jointNameList[5]);





    /*subscribe joints state topic*/
    /*advertise joint commands topic*/
    for (vector<string>::iterator it = jointsControllerNameVector.begin(); it != jointsControllerNameVector.end(); it++){
        string stateTopicName = string("/") + string(it->c_str()) + string("/state");
        string commandTopicName = string("/") + string(it->c_str()) + string("/command");

        ros::Subscriber sub = nodeHandle.subscribe(stateTopicName, 10, &PhyArmController::jointStateCallback, this);
        subscriberList.push_back(sub);

        ros::Publisher pub = nodeHandle.advertise<std_msgs::Float64>(commandTopicName,10);
        publisherList.push_back(pub);

        publisherMap.insert(pair<string,ros::Publisher>(it->c_str(), pub));

    }



    /*associate joint name with dynamixel_msgs (std::map)*/
    for (vector<string>::iterator it = jointsNameVector.begin(); it != jointsNameVector.end(); it++){
//        init joints state map
        jointsStateMap.insert(pair<string,dynamixel_msgs::JointState>(string(it->c_str()), dynamixel_msgs::JointState()));
    }



        /*Service Client*/


}

void PhyArmController::printJointsInfo() {
    cout<< "-----------Joint State-----------" <<endl;

    for (map<string,dynamixel_msgs::JointState>::iterator it = jointsStateMap.begin();it!=jointsStateMap.end();it++){
        cout << setw(30) << left <<it->first << " position:" << it->second.current_pos <<endl;
//        cout << it->second <<endl;

    }

    cout<< "-----------    END    -----------" <<endl;


}

void PhyArmController::jointStateCallback(const dynamixel_msgs::JointStateConstPtr& jointState) {
    string name = jointState->name;
    //if exist then paste it to state map
    map<string,dynamixel_msgs::JointState>::iterator jointIt = jointsStateMap.find(name);

//    means the name is found in the map
    if (jointIt != jointsStateMap.end()){
        pair<string, dynamixel_msgs::JointState> nowState(name,*jointState);
        jointsStateMap.erase(jointIt);
        jointsStateMap.insert(nowState);
    }
}


void PhyArmController::setJointPosition(int jointNum, double position) {
    ros::Publisher selPublisher = publisherList.at(jointNum);
    std_msgs::Float64 positionMsg;
    positionMsg.data = position;
    selPublisher.publish(positionMsg);


}

void PhyArmController::setJointsPosition(vector<PhyJoint> joints) {
    for (vector<PhyJoint>::iterator jointIt = joints.begin(); jointIt!=joints.end();jointIt++ ){
        setJointPosition(jointIt->jointNum,jointIt->position);
    }
}


void PhyArmController::printJointsIndex() {
    int count=0;

    cout << "-----------Joints List-----------" << endl;
        cout << setw(6) << left << "index"<<" "<< setw(30) << left << "name"      <<endl;

    for (vector<string>::iterator it = jointsControllerNameVector.begin();it!=jointsControllerNameVector.end();it++) {
        cout << setw(6) << left << count  <<" "<< setw(30) << left << it->c_str() <<endl;
        count++;
    }
    cout<< "-----------    END    -----------" <<endl;


}

void PhyArmController::closeGrabberForBall() {
    /*grabber index = 1*/
    this->setJointPosition(1,3.5);
}

void PhyArmController::openGrabberForBall() {
    /*grabber index = 1*/
    this->setJointPosition(1,2.5);
}

void PhyArmController::mirrorJointsPositionFromVrep(VrepArmController &vrepArmController, PointOffset pointOffset) {

    /*get ball position and set offset*/
    geometry_msgs::PoseStamped pose = vrepArmController.getTargetPoseRelativeToKinectReference();
    geometry_msgs::PoseStamped orgPose = pose;
    pose.pose.position.x += pointOffset.x;
    pose.pose.position.y += pointOffset.y;
    pose.pose.position.z += pointOffset.z;

    /*set target ball in vrep according to offset*/
    vrepArmController.setTargetPointRelativeToKinectReference(pose.pose.position);

    /*TODO: delay!?*/


    /*read joints position from vrep to act the physical arm*/
    for (pair<string, VrepJoint> joint : vrepArmController.getJointInfoMap()){

        string nameOfJoint  = joint.first.replace(joint.first.find("_joint"),string("_joint").length(),"_controller") ;

        ros::Publisher pub = publisherMap.find(nameOfJoint)->second;
        std_msgs::Float64 positionMsg;
        /*TODO: unit conversion between vrep and physical arm */
        positionMsg.data = joint.second.position/180.0*M_PI;
        pub.publish(positionMsg);

    }

    /*put the target ball back (in vrep)*/
    vrepArmController.setTargetPointRelativeToKinectReference(orgPose.pose.position);

}

void PhyArmController::setJointPosition(string jointName) {


}

void PhyArmController::setToCalibrationPosition() {
    for (int i = 0; i < 5 ; ++i) {
        /*set position to 0 except grabber*/
        if (i != 1)
            this->setJointPosition(i,0);
    }
}
