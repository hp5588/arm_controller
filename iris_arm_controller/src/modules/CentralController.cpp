//
// Created by brian on 12/29/15.
//

#include <modules/CentralController.h>


CentralController::CentralController(ros::NodeHandle nodeHandle, PhyArmController &phyArmController, VrepArmController &vrepArmController,
                                     BallTracker &ballTracker) {
    this->vrepArmController = &vrepArmController;
    this->ballTracker = &ballTracker;
    this->phyArmController = &phyArmController;
    this->nodeHandle = nodeHandle;

}

void CentralController::init() {
    /*ubot controller*/
    this->ubotController = new UbotController(nodeHandle);
    this->ubotController->init();

}


#define START_STATE 0
#define BALL_FINDING_STATE 1
#define BALL_FOUND_STATE 2
#define BALL_IN_REGION_STATE 3
#define BALL_GRABBING_STATE 4
#define BALL_GRABBED_STATE 5

#define PREFER_TURN_STEP 0.1 /*radian*/




void CentralController::run() {
    namedWindow( "src", CV_WINDOW_AUTOSIZE );
    int state = START_STATE;

    while (1){

        geometry_msgs::Point ballPoint;
        bool tracked;

        ballTracker->trackObj(ballPoint,tracked);
/*
        *//*TODO:cheating value, remove later*//*
        ballPoint.z+=CHEAT_Z;*/

//        cout << "ballPoint: " <<ballPoint <<endl;
//        this->move(ballPoint);

        switch (state){
            case START_STATE:{
                state = BALL_FINDING_STATE;
                break;
            }

            case BALL_FINDING_STATE:{
                if (tracked){
                    state = BALL_FOUND_STATE;



                    /*adjust direction and distance*/
//                    ubotController->turn(this->directionDifferenceFromPoint(ballPoint)+PREFER_GRAB_RADIAN);
//                    ubotController->forward(this->distanceFromPoint(ballPoint) - PREFER_GRAB_DISTANCE);
                    ubotController->move(this->directionDifferenceFromPoint(ballPoint)+PREFER_GRAB_RADIAN,this->distanceFromPoint(ballPoint) - PREFER_GRAB_DISTANCE);

                } else {
                    state = BALL_FINDING_STATE;
                    /*turning around to find ball*/
                    ubotController->turn(0.1);
                }
                break;
            }

            case BALL_FOUND_STATE:{
                if (this->ballPointInRegion(ballPoint)) {
                    state = BALL_IN_REGION_STATE;
                } else{
                    if (tracked){
                        /*find ball in different state*/
                        state = BALL_FOUND_STATE;
                        if (ubotController->status == UBOT_STATUS_MOVING){
                            /*ignore this loop and wait until movement stop*/
                        } else if (ubotController->status == UBOT_STATUS_STILL){
                            /*TODO:debug*/
                            cout << "ball point before second action"  <<endl<<ballPoint<< endl;
                            /*move to ubot into region*/
                            ubotController->move(this->directionDifferenceFromPoint(ballPoint)+PREFER_GRAB_RADIAN,this->distanceFromPoint(ballPoint) - PREFER_GRAB_DISTANCE);

//                            ubotController->turn(this->directionDifferenceFromPoint(ballPoint)+PREFER_GRAB_RADIAN);
//                            ubotController->forward(this->distanceFromPoint(ballPoint) - PREFER_GRAB_DISTANCE);

                            /*TODO: not sure if delay is necessary*/
                            usleep(10000);
                        } else{
                            cout << "ubot status exception" <<endl;
                            return;
                        }
                    } else {
                        state = BALL_FINDING_STATE;
                    }
                }
                break;
            }
            case BALL_IN_REGION_STATE:{
                if (this->ballPointInRegion(ballPoint)) {
                    /*if ball is within the region, then grab the ball*/
                    state  = BALL_GRABBING_STATE;
                } else{
                    if (tracked)
                        /*find ball in different state*/
                        state = BALL_FOUND_STATE;
                    else
                        state = BALL_FINDING_STATE;
                }
                break;
            }
            case BALL_GRABBING_STATE:{
                /*perform grab action when ubot stand still*/
                if (ubotController->status == UBOT_STATUS_STILL){
                    if (this->grabBall(ballPoint))
                        state = BALL_GRABBED_STATE;
                }
                break;
            }
            case BALL_GRABBED_STATE:{
                break;
            }



        }

        /*TODO: debug use*/
        static int lastState;
        if (state != lastState){
            this->printCentralControllerState(state);
            lastState = state;
        }


        int num = waitKey(10);
        if (num > -1)
            break;






    }

}
void CentralController::calibrate() {
    /*strech the arm to a known status*/
    phyArmController->setToCalibrationPosition();
    /*user will put the ball at arm's tip*/
    /*wait for ready-command from user*/


    cout << "press 'c' to start calibration" <<endl;
    bool tracked = false;
    geometry_msgs::Point point;
     string cmd;
    cin >> cmd;
    if (cmd.compare("c")==0){

        while(!tracked){
            ballTracker->trackObj(point,tracked);
        }
        cout << "ball position obtained"<<endl;

    } else{
        return;
    }

    /*get target position (relative to reference)*/
    geometry_msgs::Point vrepTargetPoint;
    vrepArmController->setTargetInit();
    geometry_msgs::PoseStamped pose = vrepArmController->getTargetPoseRelativeToKinectReference();


    cout <<"phy point "<<endl << point <<endl;
    cout <<"vrep point "<<endl << pose.pose.position <<endl;


    /*CAUTION:kinect coordination is different from vrep world coordination*/
    /*record and calculate the difference*/
    calibrateDifferentce.x = point.x - pose.pose.position.x;
    calibrateDifferentce.y = point.y - pose.pose.position.y;
    calibrateDifferentce.z = point.z - pose.pose.position.z;

    /*TODO:debug use*/
    cout << "--calibration different--" <<endl;
    cout << calibrateDifferentce <<endl;

}

bool CentralController::move(geometry_msgs::Point ballPoint) {

    return false;
}

bool CentralController::ballPointInRegion(geometry_msgs::Point ballPoint) {
    if ((ballPoint.x < GRAB_REGION_X_MID+GRAB_REGION_X_SIDERANGE)&&(ballPoint.x > GRAB_REGION_X_MID-GRAB_REGION_X_SIDERANGE)
        && (ballPoint.z > GRAB_REGION_Z_MIN) && (ballPoint.z < GRAB_REGION_Z_MAX)) {
        cout << "ball inside region" <<endl;
        return true;
    } else {
//        cout << "ball out of region" << ballPoint.x << "out of bound:+-" << GRAB_REGION_X_SIDERANGE<< endl;
        return false;
    }
}

bool CentralController::grabBall(geometry_msgs::Point ballPoint) {
    /*add calibration offset*/
    ballPoint.x -=calibrateDifferentce.x;
    ballPoint.y -=calibrateDifferentce.y;
    ballPoint.z -=calibrateDifferentce.z;

    /*set vrep target ball position*/
    vrepArmController->setTargetPointRelativeToKinectReference(ballPoint);

    /*TODO: implement delay here!?*/



    /*open grabber*/
    phyArmController->openGrabberForBall();

    /*set physical arm to position*/
    /*TODO: maybe separate into two steps approach [fast] [slow]*/
/*    PointOffset offset = {0,0,0.5};
    phyArmController->mirrorJointsPositionFromVrep(*vrepArmController,PointOffset{0,0,0.5});
    usleep(1000000); // 1 sec*/
    phyArmController->mirrorJointsPositionFromVrep(*vrepArmController,PointOffset{0,0,0});
    usleep(4000000); // 1 sec

    /*close grabber*/
    phyArmController->closeGrabberForBall();




    return true;
}


double CentralController::directionDifferenceFromPoint(geometry_msgs::Point ballPoint) {
    double dff = atan(ballPoint.x/ballPoint.z);
//    cout << "direction difference:" <<dff << endl;
    return -dff;
}

double CentralController::distanceFromPoint(geometry_msgs::Point ballPoint) {
    return sqrt(pow(ballPoint.x,2) + pow(ballPoint.z,2));
}

void CentralController::printCentralControllerState(int state) {
    switch (state){
        case START_STATE: {
            cout << "START_STATE" << endl;
            break;
        }
        case BALL_FINDING_STATE:
        {
            cout << "BALL_FINDING_STATE" << endl;
            break;
        }
        case BALL_FOUND_STATE:
        {
            cout << "BALL_FOUND_STATE" << endl;
            break;
        }
        case BALL_IN_REGION_STATE:
        {
            cout << "BALL_IN_REGION_STATE" << endl;
            break;
        }
        case BALL_GRABBING_STATE:
        {
            cout << "BALL_GRABBING_STATE" << endl;
            break;
        }
        case BALL_GRABBED_STATE:
        {
            cout << "BALL_GRABBED_STATE" << endl;
            break;
        }




    }

}


