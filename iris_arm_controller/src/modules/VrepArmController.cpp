//
// Created by brian on 12/29/15.
//

#include <vrep_common/simRosGetObjectHandle.h>
#include <vrep_common/simRosGetObjectPose.h>
#include <vrep_common/simRosSetObjectPosition.h>
#include <vrep_common/simRosGetJointMatrix.h>
#include <vrep_common/simRosGetJointState.h>
#include <vrep_common/simRosStartSimulation.h>
#include <ros/node_handle.h>
#include <modules/VrepArmController.h>
#include <modules/JointData.h>

VrepArmController::VrepArmController(ros::NodeHandle nodeHandle) {
    this->nodeHandle = nodeHandle;
}


bool VrepArmController::init() {
    /*Service Clients*/
    //Vrep API Service
    serviceClient_simRosGetObjectHandle = nodeHandle.serviceClient<vrep_common::simRosGetObjectHandle>("/vrep/simRosGetObjectHandle");
    serviceClient_simRosGetObjectPose = nodeHandle.serviceClient<vrep_common::simRosGetObjectPose>("/vrep/simRosGetObjectPose");
    serviceClient_simRosSetObjectPosition = nodeHandle.serviceClient<vrep_common::simRosSetObjectPosition>("/vrep/simRosSetObjectPosition");
    serviceClient_simRosGetJointMatrix = nodeHandle.serviceClient<vrep_common::simRosGetJointMatrix>("/vrep/simRosGetJointMatrix");
    serviceClient_simRosGetJointState = nodeHandle.serviceClient<vrep_common::simRosGetJointState>("/vrep/simRosGetJointState");
    serviceClient_simRosStartSimulation = nodeHandle.serviceClient<vrep_common::simRosStartSimulation>("/vrep/simRosStartSimulation");

    /*Start simulation*/
    vrep_common::simRosStartSimulation service_simRosStartSimulation;
    serviceClient_simRosStartSimulation.call(service_simRosStartSimulation);
    if (service_simRosStartSimulation.response.result<0){
        cout << "Simulation fail to start"<<endl;
        return -1;
    }


    /*joint name with handle --> map */
    char *targetNameList[] = {"target_ball"};
    char *referenceNameList[] = {"reference"};

    char *jointNameList[] = { /*"base_joint",*/ "stretch0_joint", "stretch1_joint", "rotate_joint", "sway_joint"};


    //declare getHandle srv
    vrep_common::simRosGetObjectHandle service_simRosGetObjectHandle =  vrep_common::simRosGetObjectHandle();

    //for joints handle
    for (int i = 0; i < sizeof(jointNameList) / sizeof(char*); i++) {
        service_simRosGetObjectHandle.request.objectName = jointNameList[i];
        serviceClient_simRosGetObjectHandle.call(service_simRosGetObjectHandle);
        if (service_simRosGetObjectHandle.response.handle<0){
            cout << "Can't find object with name '" << jointNameList[i] << "'" << endl;
            break;
        }
        VrepJoint joint;
        joint.handle = service_simRosGetObjectHandle.response.handle;
        jointInfoMap.insert(pair<string, VrepJoint>(jointNameList[i], joint));
    }

    //TODO: implement for only one target
    //for target handle
    ballHandle = -1;
    service_simRosGetObjectHandle.request.objectName = targetNameList[0];
    serviceClient_simRosGetObjectHandle.call(service_simRosGetObjectHandle);
    ballHandle = service_simRosGetObjectHandle.response.handle;

    //for reference handle
    kinectReferenceHandle = -1;
    service_simRosGetObjectHandle.request.objectName = referenceNameList[0];
    serviceClient_simRosGetObjectHandle.call(service_simRosGetObjectHandle);
    kinectReferenceHandle = service_simRosGetObjectHandle.response.handle;

    if (kinectReferenceHandle < 0){
        cout << "Vrep init error:" << "reference handle not found "<< endl ;
    }



    //TODO: Debug Use
    printMap(jointInfoMap);
    return false;
}

bool VrepArmController::setTargetPointRelativeToKinectReference(geometry_msgs::Point point) {
    /*TODO: check if hadle will equal to 0 when retrieval failed*/
    if (ballHandle == 0){
        cout << "ball handle not obtained" <<endl;
        return false;
    }

    vrep_common::simRosSetObjectPosition service_position = vrep_common::simRosSetObjectPosition();

    service_position.request.handle = ballHandle;
    service_position.request.position.x = point.x;
    service_position.request.position.y = point.y;
    service_position.request.position.z = point.z;
    service_position.request.relativeToObjectHandle = kinectReferenceHandle;



    //call ser vice to set point
    serviceClient_simRosSetObjectPosition.call(service_position);

    if(service_position.response.result<0)
        return false;
    return true;
}
bool VrepArmController::setTargetPointRelativeToWorldCoord(geometry_msgs::Point point) {
    /*TODO: check if hadle will equal to 0 when retreival failed*/
    if (ballHandle == 0){
        cout << "ball handle not obtained" <<endl;
        return false;
    }

    vrep_common::simRosSetObjectPosition service_position = vrep_common::simRosSetObjectPosition();

    service_position.request.handle = ballHandle;
    service_position.request.position.x = point.x;
    service_position.request.position.y = point.y;
    service_position.request.position.z = point.z;
    service_position.request.relativeToObjectHandle = -1;



    //call ser vice to set point
    serviceClient_simRosSetObjectPosition.call(service_position);

    if(service_position.response.result<0)
        return false;
    return true;
}


bool VrepArmController::getJointsPosition(map<string, VrepJoint> &jointsInfo) {

    jointsInfo.clear();

    //read the parameter of each joint from vrep
    vrep_common::simRosGetJointState service_joint;

    for (map<string, VrepJoint>::iterator pair_it = jointInfoMap.begin(); pair_it != jointInfoMap.end(); ++pair_it) {
        service_joint.request.handle = pair_it->second.handle;
        serviceClient_simRosGetJointState.call(service_joint);
        sensor_msgs::JointState jointState;
        jointState = service_joint.response.state;

//        TODO: fix exception when vrep is not opened
        float  position = (float)*jointState.position.begin()/M_PI*180;

        //put data into map
        if (service_joint.response.result<0) {
            pair_it->second.position = ARM_ERROR;
            return false;
        }
        else{
            pair_it->second.position = position;
        }

//        cout << "Joint:"<< pair_it->first <<" position:"<< position <<endl;

    }

    jointsInfo.insert(jointInfoMap.begin(), jointInfoMap.end());
    return true;
}

void VrepArmController::printMap(std::map<string, VrepJoint> &theMap) {
    for (std::map<string, VrepJoint>::iterator it = theMap.begin() ; it != theMap.end(); ++it) {
        cout << "Object Name:"<< it->first <<"  Handle:"<<it->second.handle<<endl ;
    }
}

void VrepArmController::printJointsInfo() {
    for (map<string, VrepJoint>::iterator pair_it = jointInfoMap.begin(); pair_it != jointInfoMap.end(); ++pair_it) {
        cout << "name:"<< pair_it->first <<endl;
        cout << " |" << " handle   "<< pair_it->second.handle<<endl;
        cout << " |" << " position "<< pair_it->second.position<<endl;
        cout << endl;

    }

}

double VrepArmController::getJointsPosition(int jointNum) {

    return 0;
}

map<string, VrepJoint> VrepArmController::getJointInfoMap() {
    /*update map*/
    map<string, VrepJoint> nullMap;
    this->getJointsPosition(nullMap);
    return this->jointInfoMap;
}

geometry_msgs::PoseStamped VrepArmController::getTargetPoseRelativeToKinectReference() {
    vrep_common::simRosGetObjectPose req = vrep_common::simRosGetObjectPose();
    req.request.relativeToObjectHandle = kinectReferenceHandle;
    req.request.handle = ballHandle;

    serviceClient_simRosGetObjectPose.call(req);
    geometry_msgs::PoseStamped msg;
    msg = req.response.pose;

    return msg;
}

bool VrepArmController::setTargetInit() {
    geometry_msgs::Point initPoint;
    initPoint.x = TARGET_INIT_X;
    initPoint.y = TARGET_INIT_Y;
    initPoint.z = TARGET_INIT_Z;

    this->setTargetPointRelativeToWorldCoord(initPoint);
    return true;
}

